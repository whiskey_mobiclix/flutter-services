import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

class FirebaseAppReference {
  static FirebaseApp firebaseApp;
  static int userId;

  static void connect({int userId}) async {
    WidgetsFlutterBinding.ensureInitialized();
    FirebaseAppReference.userId = userId;
    FirebaseAppReference.firebaseApp = await Firebase.initializeApp(
      options: Platform.isIOS || Platform.isMacOS
          ? FirebaseOptions(
              appId: '1:434356941684:ios:069a5ec03ee0e11b4db221',
              apiKey: 'AIzaSyAIIQOVDxOax70hTgk-aLv273HWvvH-pvI',
              projectId: 'moco360-1531821526014',
              messagingSenderId: '434356941684',
              databaseURL: 'https://moco360-1531821526014.firebaseio.com/',
            )
          : FirebaseOptions(
              appId: '1:434356941684:android:c89841f5f4b1954e4db221',
              apiKey: 'AIzaSyAIIQOVDxOax70hTgk-aLv273HWvvH-pvI',
              messagingSenderId: '434356941684',
              projectId: 'moco360-1531821526014',
              databaseURL: 'https://moco360-1531821526014.firebaseio.com/',
            ),
    );
  }
}
