class Time {
  static int utcUnixTimestamp() {
    return (DateTime.now().toUtc().millisecondsSinceEpoch / 1000).round();
  }

  static int utcMondayUnixTimestamp() {
    final monday = DateTime.now().toUtc().subtract(
        Duration(days: DateTime.now().toUtc().weekday - DateTime.monday));

    return (DateTime.utc(monday.year, monday.month, monday.day)
                .millisecondsSinceEpoch /
            1000)
        .round();
  }

  static int utcFirstDayOfTheMonthUnixTimestamp() {
    final today = DateTime.now().toUtc();

    return (DateTime.utc(today.year, today.month, 1).millisecondsSinceEpoch /
            1000)
        .round();
  }
}
