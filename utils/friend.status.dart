enum FriendStatus { NEW_REQUEST, ACCEPTED, DECLINE, OTHERS }

FriendStatus friendStatusFromInt(int value) {
  switch (value) {
    case 0:
      return FriendStatus.NEW_REQUEST;
    case 1:
      return FriendStatus.ACCEPTED;
    case 2:
      return FriendStatus.DECLINE;
    default:
      return FriendStatus.OTHERS;
  }
}

int friendStatusToInt(FriendStatus value) {
  switch (value) {
    case FriendStatus.NEW_REQUEST:
      return 0;
    case FriendStatus.ACCEPTED:
      return 1;
    case FriendStatus.DECLINE:
      return 2;
    default:
      return -1;
  }
}
