enum Gender { MALE, FEMALE, UNKNOWN }

Gender genderFromInt(int value) {
  switch (value) {
    case 1:
      return Gender.MALE;
    case 2:
      return Gender.FEMALE;
    default:
      return Gender.UNKNOWN;
  }
}

int genderToInt(Gender value) {
  switch (value) {
    case Gender.MALE:
      return 1;
    case Gender.FEMALE:
      return 2;
    default:
      return 0;
  }
}
