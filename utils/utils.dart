import 'dart:convert';

import 'package:flutter/cupertino.dart';

import '../models/level.range.model.dart';
import 'level.dart';

class Utils {
  static String clearZeroWidthSpace(String str) {
    try {
      List<int> bytes = str.toString().codeUnits;
      return utf8.decode(bytes).replaceAll("", "\u{200B}");
    } catch (e) {
      print("Error when handle utf8 with string: $str");
      print(e);
      return str.replaceAll("", "\u{200B}");
    }
  }

  static String utf8Decode(String str) {
    try {
      List<int> bytes = str.toString().codeUnits;
      return utf8.decode(bytes);
    } catch (e) {
      print("Error when handle utf8 with string: $str");
      return str;
    }
  }

  static LevelRange getLevelRange({int levelNumber}) {
    LevelRange range = new LevelRange(
      min: 99,
      colors: [Color(0xff0E1311), Color(0xff82958B), Color(0xff262B28)],
      gradientBegin: Alignment.topCenter,
      gradientEnd: Alignment.bottomCenter,
      borderColor: Color(0xff667775),
    );

    levelRangeConfiguration.forEach((o) {
      if (o.min <= levelNumber && o.max >= levelNumber) {
        range = o;
      }
    });

    return range;
  }
}
