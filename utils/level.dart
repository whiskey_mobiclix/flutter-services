import 'package:flutter/material.dart';

import '../models/level.range.model.dart';

final List<LevelRange> levelRangeConfiguration = [
  new LevelRange(
    min: 1,
    max: 11,
    colors: [Color(0xff00BD2A), Color(0xff23ED4F)],
    gradientBegin: Alignment.centerLeft,
    gradientEnd: Alignment.centerRight,
    borderColor: Colors.transparent,
  ),
  new LevelRange(
    min: 12,
    max: 22,
    colors: [Color(0xff00C5AC), Color(0xff30FFE5)],
    gradientBegin: Alignment.centerLeft,
    gradientEnd: Alignment.centerRight,
    borderColor: Colors.transparent,
  ),
  new LevelRange(
    min: 23,
    max: 33,
    colors: [Color(0xff001EBB), Color(0xff3959FF)],
    gradientBegin: Alignment.centerLeft,
    gradientEnd: Alignment.centerRight,
    borderColor: Colors.transparent,
  ),
  new LevelRange(
    min: 34,
    max: 44,
    colors: [Color(0xff7F00D2), Color(0xffB10DE1)],
    gradientBegin: Alignment.centerLeft,
    gradientEnd: Alignment.centerRight,
    borderColor: Colors.transparent,
  ),
  new LevelRange(
    min: 45,
    max: 55,
    colors: [Color(0xffA4009D), Color(0xffFF00F5)],
    gradientBegin: Alignment.centerLeft,
    gradientEnd: Alignment.centerRight,
    borderColor: Colors.transparent,
  ),
  new LevelRange(
    min: 56,
    max: 66,
    colors: [Color(0xffA4009D), Color(0xffFF00F5)],
    gradientBegin: Alignment.centerLeft,
    gradientEnd: Alignment.centerRight,
    borderColor: Colors.transparent,
  ),
  new LevelRange(
    min: 67,
    max: 77,
    colors: [Color(0xffA4009D), Color(0xffFF00F5)],
    gradientBegin: Alignment.centerLeft,
    gradientEnd: Alignment.centerRight,
    borderColor: Colors.transparent,
  ),
  new LevelRange(
    min: 78,
    max: 88,
    colors: [Color(0xff770000), Color(0xffD10000)],
    gradientBegin: Alignment.centerLeft,
    gradientEnd: Alignment.centerRight,
    borderColor: Colors.transparent,
  ),
  new LevelRange(
    min: 89,
    max: 98,
    colors: [Color(0xffFF0000), Color(0xffFF8888)],
    gradientBegin: Alignment.centerLeft,
    gradientEnd: Alignment.centerRight,
    borderColor: Colors.transparent,
  ),
];
