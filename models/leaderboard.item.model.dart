class Leader {
  String name;
  String description;
  String avatarUrl;
  int point;
  int rank;
  int userId;
  String pendantUrl;

  Leader({
    this.name = "(None)",
    this.description = "(None)",
    this.point = 0,
    this.rank = -99,
    this.avatarUrl = "",
    this.userId = -99,
    this.pendantUrl,
  });
}
