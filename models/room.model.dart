import 'package:flutterbase/core/@services/models/user.model.dart';

import 'item.model.dart';

class Room {
  String thumbnail;
  String name;
  String uid;
  int id;
  String description;
  String countryCode;
  List<User> users;
  int hostId;
  int point;
  bool isOfficial;
  int audienceNum;

  Room({
    String thumbnail,
    String name,
    String uid,
    int id,
    String description,
    String countryCode,
    List<User> users,
    int hostId,
    int point,
    bool isOfficial,
    int audienceNum,
  }) {
    this.thumbnail = thumbnail ?? "";
    this.name = name ?? "";
    this.uid = uid ?? "";
    this.id = id ?? 0;
    this.description = description ?? "";
    this.countryCode = countryCode ?? "";
    this.users = users ?? [];
    this.hostId = hostId ?? 0;
    this.point = point ?? 0;
    this.isOfficial = isOfficial ?? false;
    this.audienceNum = audienceNum ?? 0;
  }

  ///Mapping the properties
  Map<String, dynamic> _toMap() {
    return {
      'thumbnail': thumbnail,
      'name': name,
      'uid': uid,
      'id': id,
      'description': description,
      'countryCode': countryCode,
      'users': users,
      'hostId': hostId,
      'point': point,
      'isOfficial': isOfficial,
      'audienceNum': audienceNum,
    };
  }

  ///get function to get the properties of Item
  dynamic get(String propertyName) {
    var _mapRep = _toMap();
    if (_mapRep.containsKey(propertyName)) {
      return _mapRep[propertyName];
    }
    throw ArgumentError('property not found');
  }
}
