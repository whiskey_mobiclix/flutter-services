import 'package:flutterbase/core/i18n/i18n.dart';
import 'package:flutterbase/core/utils/assets_path.dart';

enum PromoEnum {
  PROMO_1,
  PROMO_2,
}

class PromoModel {
  int promoId;
  String name;
  double price;
  int coins;
  int valuePerUsd;
  int endAt;
  String inStoreId;
  PromoEnum promoType;

  PromoModel({
    this.promoId,
    this.name = "",
    this.price = 0.0,
    this.coins = 0,
    this.valuePerUsd = 0,
    this.endAt = 0,
    this.inStoreId = "",
    this.promoType = PromoEnum.PROMO_1,
  });
}

extension PromoExtension on PromoModel {
  String getImageUrl() {
    if (this.promoType == PromoEnum.PROMO_1)
      return AssetsPath.fromImagesActivities('special_offer');

    return AssetsPath.fromImagesActivities('special_offer_2');
  }

  String getPromoDescription() {
    if (this.promoType == PromoEnum.PROMO_1)
      return I18n.get("specialOffer.content1");

    return I18n.get("specialOffer.content2");
  }
}
