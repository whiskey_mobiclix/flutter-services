class Relationship {
  List<int> friends;
  List<int> requests;

  Relationship({this.friends = const [], this.requests = const []});
}
