class BroadcastModel {
  int roomId;
  int broadcasterId;
  int timestamp;
  String description;

  BroadcastModel({
    this.roomId,
    this.broadcasterId,
    this.timestamp = 0,
    this.description = "",
  });
}
