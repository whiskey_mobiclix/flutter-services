import 'package:flutter/material.dart';

class LevelRange {
  final int min;
  final int max;
  final String frameUrl;
  final List<Color> colors;
  final Alignment gradientBegin;
  final Alignment gradientEnd;
  final Color borderColor;

  LevelRange({
    this.min,
    this.max,
    this.colors,
    this.frameUrl,
    this.gradientBegin,
    this.gradientEnd,
    this.borderColor = Colors.transparent,
  });
}
