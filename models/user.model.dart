import '../utils/gender.dart';
import 'country.model.dart';
import 'level.model.dart';

enum UserTypeEnum {
  ADMIN,
  NORMAL,
  BROADCASTER,
}

class User {
  String avatarUrl;
  String fullName;
  String firstName;
  String lastName;
  String email;
  int id;
  String uid;
  Country country;
  Gender gender;
  Level level;
  int age;
  int birthDayUnixTimestamp;
  int coins;
  int gems;
  String pendantUrl;
  bool hasNewNotification;
  String userType;
  List<dynamic> roles;
  List<dynamic> gifts;

  User({
    String avatarUrl,
    String fullName,
    String firstName,
    String lastName,
    String email,
    int id,
    Country country,
    Gender gender,
    int coins,
    int gems,
    bool isPlatinum,
    Level level,
    int age,
    String uid,
    String pendantUrl,
    bool hasNewNotification,
    String userType,
    List<dynamic> roles,
    List<dynamic> gifts,
  }) {
    this.avatarUrl = avatarUrl ?? "";
    this.fullName = fullName ?? "";
    this.firstName = firstName ?? "";
    this.lastName = lastName ?? "";
    this.email = email ?? "";
    this.id = id ?? 0;
    this.country = country ?? new Country();
    this.gender = gender ?? Gender.UNKNOWN;
    this.level = level ?? new Level();
    this.age = age ?? 0;
    this.uid = uid ?? "(None)";
    this.coins = coins ?? 0;
    this.gems = gems ?? 0;
    this.pendantUrl = pendantUrl ?? null;
    this.hasNewNotification = hasNewNotification ?? false;
    this.userType = userType ?? "normal";
    this.roles = roles ?? [];
    this.gifts = gifts ?? [];
  }
}

extension userExtension on User {
  UserTypeEnum getUserTypeEnum() {
    switch (this.userType) {
      case "admin":
        return UserTypeEnum.ADMIN;
      case "bc":
        return UserTypeEnum.BROADCASTER;
      case "normal":
        return UserTypeEnum.NORMAL;
      default:
        return UserTypeEnum.NORMAL;
    }
  }
}
