import 'package:flutter/cupertino.dart';

import '../utils/utils.dart';

class Level {
  int value;
  int progress;
  List<Color> colors;
  Alignment gradientBegin;
  Alignment gradientEnd;
  String frameUrl;
  Color borderColor;

  Level({
    int value,
    int progress,
    String frameUrl,
  }) {
    this.value = value ?? 1;
    final levelRange = Utils.getLevelRange(levelNumber: this.value);

    this.progress = progress ?? 0;
    this.colors = levelRange.colors;
    this.frameUrl = frameUrl ?? levelRange.frameUrl;
    this.gradientBegin = levelRange.gradientBegin;
    this.gradientEnd = levelRange.gradientEnd;
    this.borderColor = levelRange.borderColor;
  }
}
