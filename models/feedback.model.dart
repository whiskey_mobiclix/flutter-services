class FeedbackModel {
  String content;
  String email;

  FeedbackModel({
    this.content,
    this.email,
  });
}
