enum ItemType { FREE_THUMBNAIL, GIFT, OTHERS }

// ItemType itemTypeFromString(String value) {
//   switch (value) {
//     case "free_thumbnail":
//       return ItemType.FREE_THUMBNAIL;
//     case "gift_item":
//       return ItemType.GIFT_ITEM;
//     default:
//       return ItemType.OTHERS;
//   }
// }
//
// String itemTypeToString(ItemType value) {
//   switch (value) {
//     case ItemType.FREE_THUMBNAIL:
//       return "free_thumbnail";
//     case ItemType.GIFT_ITEM:
//       return "gift_item";
//     default:
//       return "";
//   }
// }

class Item {
  int id;
  String name;
  String thumbnail;
  ItemType type;
  int amount;

  Item({
    this.id = 0,
    this.name = "",
    this.thumbnail = "",
    this.type = ItemType.OTHERS,
    this.amount = 0,
  });
}
