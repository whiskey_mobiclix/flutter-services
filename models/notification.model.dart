import 'package:firebase_database/firebase_database.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';

import '../services/communicate.service.dart';
import '../utils/firebase.dart';
import '../utils/friend.status.dart';
import 'user.model.dart';

enum NotificationType { FRIEND_REQUEST, GIFT_SEND, OTHERS }

NotificationType notificationTypeFromInt(int value) {
  switch (value) {
    case 0:
      return NotificationType.FRIEND_REQUEST;
    default:
      return NotificationType.OTHERS;
  }
}

int notificationTypeToInt(NotificationType value) {
  switch (value) {
    case NotificationType.FRIEND_REQUEST:
      return 0;
    default:
      return -1;
  }
}

class Notif {
  NotificationType type;
  int time;
  bool isRead;
  User user;
  String key;
  FriendStatus action;

  Notif({
    NotificationType type,
    int time,
    bool isRead,
    User user,
    String key,
    FriendStatus action,
  }) {
    this.type = type ?? NotificationType.OTHERS;
    this.time = time ?? DateTime.now().millisecondsSinceEpoch;
    this.isRead = isRead ?? false;
    this.user = user ?? new User();
    this.key = key ?? "";
    this.action = action;

    getIt<CommunicateService>()
        .markNotificationAsAccepted
        .stream
        .listen((senderId) {
      if (senderId == this.user.id &&
          this.type == NotificationType.FRIEND_REQUEST) {
        this.handleFriendRequest(type: FriendStatus.ACCEPTED);
      }
    });

    getIt<CommunicateService>()
        .markNotificationAsCanceled
        .stream
        .listen((senderId) {
      if (senderId == this.user.id &&
          this.type == NotificationType.FRIEND_REQUEST) {
        this.handleFriendRequest(type: FriendStatus.DECLINE);
      }
    });
  }

  void remove() {
    final FirebaseDatabase database =
        FirebaseDatabase(app: FirebaseAppReference.firebaseApp);

    database
        .reference()
        .child("test_1")
        .child("notifications")
        .child(FirebaseAppReference.userId.toString())
        .child(this.key)
        .remove();
  }

  void read() {
    final FirebaseDatabase database =
        FirebaseDatabase(app: FirebaseAppReference.firebaseApp);

    database
        .reference()
        .child("test_1")
        .child("notifications")
        .child(FirebaseAppReference.userId.toString())
        .child(this.key)
        .update({"is_read": true});
  }

  void handleFriendRequest({FriendStatus type}) {
    final FirebaseDatabase database =
        FirebaseDatabase(app: FirebaseAppReference.firebaseApp);

    database
        .reference()
        .child("test_1")
        .child("notifications")
        .child(FirebaseAppReference.userId.toString())
        .child(this.key)
        .update({
      "is_read": true,
      "action": friendStatusToInt(type),
      "was_action": true,
    });
  }
}
