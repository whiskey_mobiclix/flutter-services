import 'dart:convert';

import 'package:http/http.dart' as http;

import '../mockup/token.mockup.dart';

class Request {
  static String base = "https://pulse-api-v2.mocogateway.com";

  // static final String base = "https://minimal-node-prj.herokuapp.com";
  // static final String alt = "http://306d0ccf7588.ngrok.io";

  // static String token = mockupToken;
  static String token = "";

  static Future<http.Response> get({String url}) async {
    final uri = Uri.parse(url);

    return await http.get(
      uri,
      headers: _RequestHeaders.data(),
    );
  }

  static Future<http.Response> put({
    String url,
    Map<String, dynamic> data,
  }) async {
    final uri = Uri.parse(url);

    return await http.put(
      uri,
      headers: _RequestHeaders.data(),
      body: json.encode(data),
    );
  }

  static Future<http.Response> post({
    String url,
    Map<String, dynamic> data,
  }) async {
    final uri = Uri.parse(url);

    return await http.post(
      uri,
      headers: _RequestHeaders.data(),
      body: json.encode(data),
    );
  }

  static void setToken(String tkn) {
    Request.token = tkn;
  }

  static void setDomain(bool isProduction) {
    if (isProduction)
      Request.base = "https://pulse-api-v2.mocogateway.com";
    else
      Request.base = "https://stag-pulse-api-v2.mocogateway.com";
  }
}

class _RequestHeaders {
  static Map<String, String> data() {
    return {
      "Device-Id": "Test Device",
      "Device-Info": "Test Device",
      "User-Agent": "PostmanRuntime/7.26.5",
      "Authorization": "Bearer ${Request.token}",
      "Content-Type": "application/json; charset=UTF-8",
    };
  }
}
