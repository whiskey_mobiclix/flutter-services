final Map<String, dynamic> MockupUserDetailsResponse = {
  "status": 1,
  "data": {
    "user_id": 11754316,
    "email": "test.triet.6@gmail.com",
    "first_name": "6",
    "last_name": "Test",
    "full_name": "6Test",
    "gender": 2,
    "user_type": "Normal",
    "country": "",
    "avatar_url":
        "https://stag-pulse-api-v2.mocogateway.com/static/pulse-mena/document/1094.jpeg",
    "cover_url": "",
    "chosen_pendant_url": "",
    "completed_settings": true,
    "coins_amount": 0,
    "coins_spent": 0,
    "gems_amount": 0,
    "level": 0,
    "xp": 0,
    "gifts_sent": 0,
    "visitors": 0,
    "gifts": {},
    "user_interests": {
      "interests": [
        {
          "id": 25,
          "name": "Games",
          "url":
              "https://staging-gapi.mocogateway.com/static/pulse-mena/interest/Games.png"
        },
        {
          "id": 28,
          "name": "Love\u0026Dating",
          "url":
              "https://staging-gapi.mocogateway.com/static/pulse-mena/interest/LoveDating.png"
        }
      ]
    }
  }
};
