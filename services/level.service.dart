import 'dart:convert';

import '../models/level.range.model.dart';
import '../request/request.dart';
import '../utils/map.dart';
import '../utils/utils.dart';

class LevelService {
  static Future<List<LevelRange>> ranges() async {
    var response;

    try {
      response = await Request.get(
          url: "${Request.base}/v1/pulse-mena/tiers");
    } catch (e) {
      return [];
    }

    if (response.statusCode != 200) {
      return [];
    }

    final res = json.decode(response.body);


    if (res["data"] is List) {

      List<LevelRange> results = [];
      int minLevel = 1;
      for (int i = 0; i < res["data"].length; i++) {
        final mp = new CustomMap(res["data"][i]);

        final levelRange = Utils.getLevelRange(levelNumber: minLevel + 1);

        results.add(new LevelRange(
          min: minLevel,
          max: minLevel == 99 ? null : mp.get("max_level"),
          frameUrl: mp.get("frame_image_url.frame_image"),
          colors: levelRange.colors,
          gradientBegin: levelRange.gradientBegin,
          gradientEnd: levelRange.gradientEnd,
          borderColor: levelRange.borderColor,
        ));

        minLevel = mp.get("max_level") + 1;
      }

      return results;
    }

    return [];
  }
}
