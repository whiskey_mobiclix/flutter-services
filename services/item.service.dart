import 'dart:convert';

import '../models/item.model.dart';
import '../request/request.dart';
import '../utils/map.dart';

class ItemService {
  // static Future<List<Item>> list({ItemType type}) async {
  //   var response;
  //
  //   try {
  //     response = await Request.get(
  //         url: "${Request.base}/v1/engagement/gift?" +
  //             "product=pulse-mena" +
  //             "&type_of_gift=${itemTypeToString(type) ?? ""}");
  //   } catch (e) {
  //     return [];
  //   }
  //
  //   if (response.statusCode != 200) {
  //     return [];
  //   } else {
  //     final res = json.decode(response.body);
  //
  //     if (res["data"] is List &&
  //         res["data"].length > 0 &&
  //         res["data"][0]["gifts"] is List) {
  //       List<Item> items = [];
  //
  //       res["data"][0]["gifts"].forEach((e) {
  //         final mp = new CustomMap(e);
  //         items.add(new Item(
  //           id: mp.get("id"),
  //           name: mp.get("name"),
  //           type: type,
  //           url: mp.get("extension.thumb_url"),
  //         ));
  //       });
  //
  //       return items;
  //     }
  //   }
  //
  //   return [];
  // }

  static Future<List<Item>> receivedGiftListByUserId({int id}) async {
    var response;

    try {
      response = await Request.get(
          url: "${Request.base}/service/user/bag?user_id=$id");
    } catch (e) {
      return [];
    }

    if (response.statusCode != 200) {
      return [];
    } else {
      final res = json.decode(response.body);

      if (res["data"]["gifts"] is List && res["data"]["gifts"].length > 0) {
        final receivedFromAnotherUserItems = res["data"]["gifts"]
            .where((e) => e["category_name"] == 'received_from_another_user')
            .toList()[0]["items"];
        List<Item> items = [];

        receivedFromAnotherUserItems.forEach((e) {
          final mp = new CustomMap(e);

          items.add(
            new Item(
              id: mp.get("id"),
              name: mp.get("name"),
              type: ItemType.GIFT,
              thumbnail: mp.get("thumbnail"),
              amount: mp.get("amount"),
            ),
          );
        });

        return items;
      }
    }

    return [];
  }
}
