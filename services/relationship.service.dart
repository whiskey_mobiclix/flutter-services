import 'dart:convert';

import '../models/relationship.model.dart';
import '../request/request.dart';
import '../utils/friend.status.dart';

class RelationshipService {
  static Future<Relationship> list() async {
    var response;

    try {
      response = await Request.get(
        url:
            "${Request.base}/v1/user/relationship?fields[]=friend_normal&fields[]=friend_favourite&fields[]=requested",
      );
    } catch (e) {
      return new Relationship();
    }

    if (response.statusCode != 200) {
      return new Relationship();
    } else {
      final res = json.decode(response.body);
      if (res["data"] is Map) {
        List<int> friends = [];
        List<int> requests = [];

        if (res["data"]["friend_normal"] is List) {
          res["data"]["friend_normal"].forEach((e) {
            friends.add(e);
          });
        }
        if (res["data"]["friend_favourite"] is List) {
          res["data"]["friend_favourite"].forEach((e) {
            friends.add(e);
          });
        }

        if (res["data"]["requested"] is List) {
          res["data"]["requested"].forEach((e) {
            requests.add(e);
          });
        }

        return new Relationship(
          friends: friends,
          requests: requests,
        );
      }
    }

    return new Relationship();
  }

  static Future<bool> accept({int senderId}) async {
    var response;

    try {
      response = await Request.put(
          url: "${Request.base}/pulse-mena/v1/user/friend-request",
          data: {
            "user_id": senderId,
            "status": friendStatusToInt(FriendStatus.ACCEPTED),
          });
    } catch (e) {
      return false;
    }

    if (response.statusCode != 200) {
      return false;
    } else {
      final res = json.decode(response.body);
      if (res["status"] == 1) {
        return true;
      }
    }

    return false;
  }

  static Future<bool> decline({int senderId}) async {
    var response;

    try {
      response = await Request.put(
          url: "${Request.base}/pulse-mena/v1/user/friend-request",
          data: {
            "user_id": senderId,
            "status": friendStatusToInt(FriendStatus.DECLINE),
          });
    } catch (e) {
      return false;
    }

    if (response.statusCode != 200) {
      return false;
    } else {
      final res = json.decode(response.body);
      if (res["status"] == 1) {
        return true;
      }
    }

    return false;
  }
}
