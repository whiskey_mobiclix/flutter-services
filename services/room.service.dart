import 'dart:async';
import 'dart:convert';

import '../models/item.model.dart';
import '../models/room.model.dart';
import '../request/request.dart';
import '../utils/map.dart';
import '../utils/utils.dart';
import 'item.service.dart';

class RoomService {
  static Future<List<Room>> list({
    String countryCode = "",
    String search = "",
    String sortField = "created_at",
    String sortType = "desc",
    int lastId = 0,
    int page = 1,
    int limit = 10,
  }) async {
    var response;

    // await Future.delayed(const Duration(milliseconds: 100), () {
    //   print("Request room list");
    // });

    try {
      response = await Request.post(
        url: "${Request.base}/v1/haki/community/rooms",
        data: {
          "sort_by": sortField,
          "country": countryCode,
          "keyword": search,
          "page": page,
          "limit": limit,
        },
      );
    } catch (e) {
      return [];
    }

    if (response.statusCode != 200) {
      return [];
    } else {
      final res = json.decode(response.body);
      // final thumbnails = await ItemService.list(type: ItemType.FREE_THUMBNAIL);

      List<Room> rooms = [];
      if (res is Map && res["data"] is List) {
        res["data"].forEach((e) {
          if (sortField != "created_at" || e["is_official"] != true) {
            // for (int i = 0; i < 10; i++)
            rooms.add(new Room(
              id: e["id"],
              hostId: e["host_id"],
              name: Utils.utf8Decode(e["name"]),
              description: Utils.utf8Decode(e["description"]),
              countryCode: e["country"],
              thumbnail: e["cover"] == null
                  ? ""
                  : e["cover"]['item_info'] == null
                      ? ""
                      : e["cover"]["item_info"]["thumbnail"],
              uid: "${e["id"]}",
              point: e["points"],
              isOfficial: e["is_official"] ?? false,
            ));
          }
        });

        return rooms;
      }
    }

    return [];
  }

  static Future<List<Room>> recentlyList({
    int lastId = 0,
    int page = 1,
    int limit = 10,
    int userId,
  }) async {
    var response;

    try {
      response = await Request.post(
        url: "${Request.base}/v1/haki/community/room/recently-joined",
        data: {
          "country": "",
          "keyword": "",
          "page": page,
          "limit": limit,
        },
      );
    } catch (e) {
      return [];
    }

    if (response.statusCode != 200) {
      return [];
    } else {
      final res = json.decode(response.body);
      // final thumbnails = await ItemService.list(type: ItemType.FREE_THUMBNAIL);

      List<Room> rooms = [];
      if (res is Map && res["data"] is List) {
        res["data"].forEach((e) {
          // for (int i = 0; i < 5; i++)
          rooms.add(new Room(
            id: e["id"],
            hostId: e["host_id"],
            name: Utils.utf8Decode(e["name"]),
            description: Utils.utf8Decode(e["description"]),
            countryCode: e["country"],
            thumbnail: e["cover"] == null
                ? ""
                : e["cover"]['item_info'] == null
                    ? ""
                    : e["cover"]["item_info"]["thumbnail"],
            uid: "${e["id"]}",
            point: e["points"],
            isOfficial: e["is_official"] ?? false,
          ));
        });

        return rooms;
      }
    }

    return [];
  }

  static Future<Room> roomById({int id}) async {
    var response;

    try {
      response =
          await Request.get(url: "${Request.base}/service/room?host_id=$id");
    } catch (e) {
      return null;
    }

    final res = json.decode(response.body);

    if (response.statusCode != 200) {
      if (res["code"] == 3) return null;
      return new Room();
    } else {
      if (res["data"] is Map) {
        final mp = new CustomMap(res["data"]);

        return new Room(
          thumbnail: mp.get("cover")["thumbnail"],
          name: Utils.utf8Decode(mp.get("name")),
          uid: "${mp.get("id")}",
          description: Utils.utf8Decode(mp.get("description")),
          countryCode: mp.get("country"),
          hostId: mp.get("host_id"),
          point: mp.get("point"),
          isOfficial: mp.get("is_official") ?? false,
        );
      }
    }

    return null;
  }
}
