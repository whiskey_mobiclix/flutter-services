import 'dart:convert';

import 'package:flutterbase/core/@services/models/item.model.dart';

import '../models/country.model.dart';
import '../models/level.model.dart';
import '../models/user.model.dart';
import '../request/request.dart';
import '../utils/gender.dart';
import '../utils/map.dart';
import '../utils/utils.dart';

class UserService {
  static Future<User> getDetailsById({dynamic id}) async {
    print("user id $id");

    String url;

    if (id == "me")
      url = "${Request.base}/v1/haki/community/user/me";
    else
      url = "${Request.base}/v1/haki/community/user-profile/$id";

    var response;

    try {
      response = await Request.get(url: url);
    } catch (e) {
      return new User();
    }

    if (response.statusCode != 200) {
      throw "Error when get user details: ID = $id";
    } else {
      final responseBody = json.decode(response.body);

      if (responseBody["data"] != null) {
        final res = responseBody["data"];

        try {
          List<Item> gifts = [];

          if (res["gifts"] is Map && res["gifts"]["gifts"] is List) {
            dynamic giftsList = res["gifts"]["gifts"];
            res["gifts"]["gifts"].asMap().forEach((index, ele) {
              if (gifts.isNotEmpty) {
                Item existedGift = gifts.firstWhere(
                  (gift) => gift.id == giftsList[index]["id"],
                  orElse: () => null,
                );

                if (existedGift != null)
                  gifts[gifts.indexOf(existedGift)].amount +=
                      giftsList[index]["amount"];
                else
                  gifts.add(new Item(
                    id: giftsList[index]["id"] ?? 0,
                    amount: giftsList[index]["amount"] ?? 0,
                    name: giftsList[index]["name"] ?? "",
                    thumbnail: giftsList[index]["thumbnail"] ?? "",
                  ));
              } else
                gifts.add(new Item(
                  id: giftsList[index]["id"] ?? 0,
                  amount: giftsList[index]["amount"] ?? 0,
                  name: giftsList[index]["name"] ?? "",
                  thumbnail: giftsList[index]["thumbnail"] ?? "",
                ));
            });
          }

          User user = new User(
            avatarUrl: res["avatar_url"],
            fullName: Utils.utf8Decode(res["full_name"]),
            firstName: Utils.utf8Decode(res["first_name"]),
            lastName: Utils.utf8Decode(res["last_name"]),
            email: res["email"],
            id: res["user_id"],
            country: new Country(code: res["country"]),
            gender: genderFromInt(res["gender"]),
            age: res["age"] ?? 0,
            coins: res["coins_amount"] ?? 0,
            gems: res["gems_amount"] ?? 0,
            // pendantUrl: res["chosen_pendant_url"],
            pendantUrl: res["equipped_items"] != null &&
                    res["equipped_items"]["pendant"] != null &&
                    res["equipped_items"]["pendant"]["item_info"] != null &&
                    res["equipped_items"]["pendant"]["item_info"]["id"] != null
                ? res["equipped_items"]["pendant"]["item_info"]["thumbnail"]
                : "",
            level: new Level(
              value: res["level"],
            ),
            gifts: gifts,
            userType: res["user_type"],
          );

          return user;
        } catch (e) {
          print(e);
        }
      }
    }

    return new User();
  }

  static Future<User> getCustomerServiceUser() async {
    String url = "${Request.base}/v1/haki/community/user-profile/1";

    var response;

    try {
      response = await Request.get(url: url);
    } catch (e) {
      return new User();
    }

    if (response.statusCode != 200) {
      throw "Error when get customer service user details";
    } else {
      final responseBody = json.decode(response.body);

      if (responseBody["data"] != null) {
        final res = responseBody["data"];

        try {
          List<Item> gifts = [];

          if (res["gifts"] is Map && res["gifts"]["gifts"] is List) {
            dynamic giftsList = res["gifts"]["gifts"];
            res["gifts"]["gifts"].asMap().forEach((index, ele) {
              if (gifts.isNotEmpty) {
                Item existedGift = gifts.firstWhere(
                  (gift) => gift.id == giftsList[index]["id"],
                  orElse: () => null,
                );

                if (existedGift != null)
                  gifts[gifts.indexOf(existedGift)].amount +=
                      giftsList[index]["amount"];
                else
                  gifts.add(new Item(
                    id: giftsList[index]["id"] ?? 0,
                    amount: giftsList[index]["amount"] ?? 0,
                    name: giftsList[index]["name"] ?? "",
                    thumbnail: giftsList[index]["thumbnail"] ?? "",
                  ));
              } else
                gifts.add(new Item(
                  id: giftsList[index]["id"] ?? 0,
                  amount: giftsList[index]["amount"] ?? 0,
                  name: giftsList[index]["name"] ?? "",
                  thumbnail: giftsList[index]["thumbnail"] ?? "",
                ));
            });
          }

          User user = new User(
            avatarUrl: res["avatar_url"],
            fullName: Utils.utf8Decode(res["full_name"]),
            firstName: Utils.utf8Decode(res["first_name"]),
            lastName: Utils.utf8Decode(res["last_name"]),
            email: res["email"],
            id: res["user_id"],
            country: new Country(code: res["country"]),
            gender: genderFromInt(res["gender"]),
            age: res["age"] ?? 0,
            coins: res["coins_amount"] ?? 0,
            gems: res["gems_amount"] ?? 0,
            // pendantUrl: res["chosen_pendant_url"],
            pendantUrl: res["equipped_items"] != null &&
                    res["equipped_items"]["pendant"] != null &&
                    res["equipped_items"]["pendant"]["item_info"] != null &&
                    res["equipped_items"]["pendant"]["item_info"]["id"] != null
                ? res["equipped_items"]["pendant"]["item_info"]["thumbnail"]
                : "",
            level: new Level(
              value: res["level"],
            ),
            gifts: gifts,
          );

          return user;
        } catch (e) {
          print(e);
        }
      }
    }

    return new User();
  }
}
