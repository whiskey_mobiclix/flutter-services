import 'dart:convert';

import '../models/leaderboard.item.model.dart';
import '../request/request.dart';
import '../utils/map.dart';
import '../utils/time.dart';
import '../utils/utils.dart';
import 'room.service.dart';

enum LeaderboardFilterType {
  // ROOM,
  BUYER,
  // GIFT_SENT,
  GIFT_RECEIVED,
  // GEM_RECEIVED,
  WINNER_COIN,
  // OTHERS
}

LeaderboardFilterType leaderboardFilterTypeFromString(String value) {
  switch (value) {
    case "wealthy":
      return LeaderboardFilterType.BUYER;
    // case "gift_sender":
    //   return LeaderboardFilterType.GIFT_SENT;
    case "topreceivers":
      return LeaderboardFilterType.GIFT_RECEIVED;
    case "winners":
      return LeaderboardFilterType.WINNER_COIN;
    // case "receive_gem":
    //   return LeaderboardFilterType.GEM_RECEIVED;
    // case "room":
    //   return LeaderboardFilterType.ROOM;
    default:
      return LeaderboardFilterType.BUYER;
  }
}

String leaderboardFilterTypeToString(LeaderboardFilterType value) {
  switch (value) {
    case LeaderboardFilterType.BUYER:
      return "wealthy";
    // case LeaderboardFilterType.GIFT_SENT:
    //   return "gift_sender";
    case LeaderboardFilterType.GIFT_RECEIVED:
      return "topreceivers";
    case LeaderboardFilterType.WINNER_COIN:
      return "winners";
    // case LeaderboardFilterType.GEM_RECEIVED:
    //   return "receive_gem";
    // case LeaderboardFilterType.ROOM:
    //   return "room";
    default:
      return "wealthy";
  }
}

enum LeaderboardTimeRange { ALL_TIME, WEEKLY, MONTHLY, DAILY }

int leaderboardTimeRangeToUnixTimestamp(LeaderboardTimeRange value) {
  switch (value) {
    case LeaderboardTimeRange.WEEKLY:
      return Time.utcMondayUnixTimestamp();
    case LeaderboardTimeRange.MONTHLY:
      return Time.utcFirstDayOfTheMonthUnixTimestamp();
    default:
      return 1;
  }
}

String leaderboardTimestampToString(LeaderboardTimeRange value) {
  switch (value) {
    case LeaderboardTimeRange.WEEKLY:
      return "weekly";
    case LeaderboardTimeRange.MONTHLY:
      return "monthly";
    case LeaderboardTimeRange.DAILY:
      return "daily";
    default:
      return "weekly";
  }
}

class LeaderboardService {
  static Future<List<Leader>> list({
    LeaderboardFilterType type,
    LeaderboardTimeRange timeRange,
  }) async {
    // if (type == LeaderboardFilterType.ROOM) {
    //   final rooms = await RoomService.list(sortField: "point", limit: 50);
    //   List<Leader> results = [];
    //
    //   for (int i = 0; i < rooms.length; i++) {
    //     final room = rooms[i];
    //     results.add(new Leader(
    //       name: room.name,
    //       description: room.description,
    //       point: room.point,
    //       avatarUrl: room.thumbnail.url,
    //       userId: room.hostId,
    //       rank: i + 1,
    //     ));
    //   }
    //
    //   return results;
    // }

    var response;

    try {
      response = await Request.get(
          url: "${Request.base}/v1/haki/leaderboard" +
              "/${leaderboardFilterTypeToString(type)}" +
              "/${leaderboardTimestampToString(timeRange)}");

      if (response.statusCode != 200) return [];

      final res = json.decode(response.body);

      if (res["data"]["toppers"] != null && res["data"]["toppers"] is List) {
        List<Leader> results = [];

        res["data"]["toppers"].asMap().forEach((index, o) {
          int point = 0;

          switch (type) {
            case LeaderboardFilterType.GIFT_RECEIVED:
              point = o["leaderboard_gifts_received"];
              break;
            // case LeaderboardFilterType.GIFT_SENT:
            //   point = o["gifts_sent"];
            //   break;
            case LeaderboardFilterType.BUYER:
              point = o["leaderboard_coins_spent"];
              break;
            case LeaderboardFilterType.WINNER_COIN:
              point = o["leaderboard_luckyhaki"];
              break;
            default:
              point = 0;
          }

          results.add(new Leader(
            name: Utils.utf8Decode(o["full_name"]),
            avatarUrl: o["avatar_url"],
            userId: o["user_id"],
            rank: index + 1,
            point: point,
            pendantUrl: o["chosen_pendant_url"],
            // pendantUrl: o["equipped_items"] != null &&
            //     o["equipped_items"]["pendant"] != null &&
            //     o["equipped_items"]["pendant"]["item_info"] != null &&
            //     o["equipped_items"]["pendant"]["item_info"]["id"] != null
            //     ? o["equipped_items"]["pendant"]["item_info"]["thumbnail"]
            //     : "",
          ));
        });

        return results;
      } else
        return [];
    } catch (e) {
      return [];
    }

    return [];
  }

  static Future<Map<String, List<Leader>>> all({
    LeaderboardTimeRange timeRange,
  }) async {
    var response;

    try {
      response = await Request.get(
          url: "${Request.base}/service/leaderboard/all?" +
              "range=${leaderboardTimestampToString(timeRange)}");

      if (response.statusCode != 200) return {};

      final res = json.decode(response.body);

      if (res["data"] is Map) {
        Map<String, List<Leader>> results = {};

        if (res["data"]
            .containsKey("wealthy")) if (res["data"]["wealthy"].length > 0) {
          List<Leader> tempData = [];

          res["data"]["wealthy"].forEach((o) {
            tempData.add(new Leader(
              name: Utils.utf8Decode(o["full_name"]),
              avatarUrl: o["avatar_url"],
              userId: o["id"],
              rank: o["rank"],
              point: o["coins_spent"],
              pendantUrl:
                  o["equipment"] != null && o["equipment"]["pendant"] != null
                      ? o["equipment"]["pendant"]["thumbnail"]
                      : o["default_pendant_url"],
            ));
          });

          results["wealthy"] = tempData;
        } else
          results["wealthy"] = [];

        if (res["data"].containsKey(
            "gift_sender")) if (res["data"]["gift_sender"].length > 0) {
          List<Leader> tempData = [];

          res["data"]["gift_sender"].forEach((o) {
            tempData.add(new Leader(
              name: Utils.utf8Decode(o["full_name"]),
              avatarUrl: o["avatar_url"],
              userId: o["id"],
              rank: o["rank"],
              point: o["gifts_sent"],
              pendantUrl:
                  o["equipment"] != null && o["equipment"]["pendant"] != null
                      ? o["equipment"]["pendant"]["thumbnail"]
                      : o["default_pendant_url"],
            ));
          });

          results["gift_sender"] = tempData;
        } else
          results["gift_sender"] = [];

        if (res["data"].containsKey(
            "gift_receiver")) if (res["data"]["gift_receiver"].length > 0) {
          List<Leader> tempData = [];

          res["data"]["gift_receiver"].forEach((o) {
            tempData.add(new Leader(
              name: Utils.utf8Decode(o["full_name"]),
              avatarUrl: o["avatar_url"],
              userId: o["id"],
              rank: o["rank"],
              point: o["gifts_received"],
              pendantUrl:
                  o["equipment"] != null && o["equipment"]["pendant"] != null
                      ? o["equipment"]["pendant"]["thumbnail"]
                      : o["default_pendant_url"],
            ));
          });

          results["gift_receiver"] = tempData;
        } else
          results["gift_receiver"] = [];

        return results;
      }
    } catch (e) {
      return {};
    }

    return {};
  }
}
