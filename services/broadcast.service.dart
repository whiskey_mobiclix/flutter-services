import 'dart:convert';

import '../models/broadcast.model.dart';
import '../request/request.dart';

class BroadcastService {
  static Future<List<BroadcastModel>> schedules() async {
    var response;

    try {
      response = await Request.get(
        url: "${Request.base}/v1/haki/broadcaster/schedules",
      );

      if (response.statusCode != 200) {
        return [];
      }

      final res = json.decode(response.body);

      if (res["data"]["schedules"] != null &&
          res["data"]["schedules"] is List) {
        dynamic schedules = res["data"]["schedules"];

        List<BroadcastModel> results = [];

        schedules.forEach((o) {
          results.add(new BroadcastModel(
            roomId: o["room_id"],
            broadcasterId: o["broadcaster_id"],
            timestamp: int.parse(o["time"] ?? "0"),
            description: o["description"],
          ));
        });

        return results;
      } else
        return [];
    } catch (e) {
      return [];
    }
  }
}
