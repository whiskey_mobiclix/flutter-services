import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterbase/configs/general.config.dart';
import 'package:flutterbase/core/dependencies/dependenciesInjection.dart';
import 'package:flutterbase/core/routes/routes.dart';
import 'package:flutterbase/providers/general.provider.dart';
import 'package:flutterbase/providers/me.provider.dart';
import 'package:flutterbase/providers/room.provider.dart';
import 'package:rxdart/rxdart.dart';

import '../request/request.dart';

class CommunicateService {
  MethodChannel _channel;
  BuildContext context;

  ReplaySubject<void> _reloadMyRoom = ReplaySubject<void>();

  Stream<void> get reloadMyRoomListener => _reloadMyRoom.stream;

  ReplaySubject<void> _reloadMyProfile = ReplaySubject<void>();

  Stream<void> get reloadMyProfileListener => _reloadMyProfile.stream;

  ReplaySubject<int> markNotificationAsAccepted = ReplaySubject<int>();

  ReplaySubject<int> markNotificationAsCanceled = ReplaySubject<int>();

  MethodChannel get channel => _channel;

  CommunicateService() {
    _channel = MethodChannel("pulse-mena", JSONMethodCodec());
    _channel.setMethodCallHandler((call) async {
      switch (call.method) {
        case "setToken":
          final token = call.arguments["token"];
          final isProduction = call.arguments["isProduction"];
          final lang = call.arguments["language"];
          final user = call.arguments["user"];
          Request.setToken(token);
          Request.setDomain(isProduction);
          getIt<GeneralConfig>().setLanguage(lang ?? "en");
          getIt<GeneralProvider>().setLanguage(lang ?? "en");
          getIt<GeneralProvider>().setProductionStatus(isProduction);
          getIt<MeProvider>().setUserData(data: user.toString());
          // _reloadMyProfile.add(null);
          break;

        case "setLanguage":
          final lang = call.arguments["language"];
          getIt<GeneralConfig>().setLanguage(lang ?? "en");
          getIt<GeneralProvider>().setLanguage(lang ?? "en");
          break;

        case "setUserInfo":
          final user = call.arguments["user"];
          getIt<MeProvider>().setUserData(data: user?.toString());
          break;

        case "setMyRoom":
          final myRoom = call.arguments["myRoom"];
          getIt<RoomProvider>().setMyRoom(data: myRoom?.toString());
          break;

        case "setMyPromo":
          final myPromo = call.arguments["myPromo"];
          getIt<GeneralProvider>().setMyPromo(myPromo?.toString());
          break;

        case "updateAudienceData":
          final data = call.arguments["audienceData"];
          getIt<RoomProvider>().updateWholeAudienceData(data);
          break;

        case "reloadMyRoom":
          _reloadMyRoom.add(null);
          break;

        case "reloadMyProfile":
          _reloadMyProfile.add(null);
          break;

        case "markNotificationAsAccepted":
          final senderId = call.arguments["senderId"];
          markNotificationAsAccepted.add(int.parse(senderId));
          break;

        case "markNotificationAsCanceled":
          final senderId = call.arguments["senderId"];
          markNotificationAsCanceled.add(int.parse(senderId));
          break;

        case "replaceRoute":
          final route = call.arguments["route"];

          Navigator.pushReplacement(
            context,
            PageRouteBuilder(
              pageBuilder: (_, __, ___) =>
                  new MyRouter().routes[route](context),
              transitionDuration: Duration(seconds: 0),
            ),
          );

          break;
      }

      return null;
    });
  }

  navigate({String route, dynamic args}) {
    _channel.invokeMethod("navigate", {"route": route, "args": args});
  }

  request({String requestName, dynamic args}) {
    _channel.invokeMethod(requestName, {"args": args});
  }

  requestHideNavigationBar() {
    _channel.invokeMethod("hideNavigationBar", {"args": {}});
  }

  requestShowNavigationBar() {
    _channel.invokeMethod("showNavigationBar", {"args": {}});
  }

  navigateBack() {
    SystemNavigator.pop(animated: true);
  }
}
