import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

import '../models/notification.model.dart';
import '../models/user.model.dart';
import '../utils/firebase.dart';
import '../utils/friend.status.dart';
import '../utils/map.dart';

class NotificationService {
  static Query ref() {
    final FirebaseDatabase database =
        FirebaseDatabase(app: FirebaseAppReference.firebaseApp);

    return database
        .reference()
        .child("test_1")
        .child("notifications")
        .child(FirebaseAppReference.userId.toString())
        .limitToLast(50);
  }

  static StreamBuilder listener({
    Widget Function(BuildContext context, List<Notif>, bool) builder,
  }) {
    return StreamBuilder(
        stream: NotificationService.ref().onValue,
        builder: (BuildContext context, snap) {
          List<Notif> list = [];

          if (snap.hasError) {
            return builder(context, list, true);
          }

          if (!snap.hasData) {
            return builder(context, list, false);
          }

          if (snap.data.snapshot.value != null) {
            snap.data.snapshot.value.forEach((key, o) {
              final mp = new CustomMap(Map<String, dynamic>.from(o));
              if (mp.get("type") ==
                  notificationTypeToInt(NotificationType.FRIEND_REQUEST)) {
                list.add(new Notif(
                  key: key,
                  type: NotificationType.FRIEND_REQUEST,
                  isRead: mp.get("is_read"),
                  time: mp.get("created_at"),
                  action: friendStatusFromInt(mp.get("action")),
                  user: new User(
                    firstName: mp.get("data.friend_first_name"),
                    lastName: mp.get("data.friend_last_name"),
                    fullName:
                        "${mp.get("data.friend_first_name")} ${mp.get("data.friend_last_name")}",
                    avatarUrl: mp.get("data.friend_avatar"),
                    id: int.parse(mp.get("data.friend_user_id")),
                  ),
                ));
              }
            });

            list.sort((a, b) => b.time.compareTo(a.time));

            return builder(context, list, true);
          }

          return builder(context, list, true);
        });
  }
}
