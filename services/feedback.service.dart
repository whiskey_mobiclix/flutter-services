import '../models/feedback.model.dart';
import '../request/request.dart';

class FeedbackService {
  static Future<bool> sendFeedback({FeedbackModel data}) async {
    var response;

    try {
      response = await Request.post(
        url: "${Request.base}/service/feedback",
        data: {
          "email": data.email,
          "content": data.content,
        },
      );
    } catch (e) {
      return false;
    }

    if (response.statusCode != 200) {
      return false;
    } else
      return true;
  }
}
